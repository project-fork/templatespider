## 所见网站，皆可为我所用－templatespider
看好哪个网站，指定好URL，自动扒下来做成 html模版。并且所下载的css、js、图片、html文件会自动分好类保存到特定文件夹！  
然后使用 模版计算工具，自动将 html模版计算合成为 [网市场云建站系统](https://gitee.com/mail_osc/wangmarket) 可用的模版。
<br/> 
项目最初目的为 [网市场云建站系统（CMS建站系统）](https://gitee.com/mail_osc/wangmarket) 因模版量少而做。用了之前写的 [xnx3](https://gitee.com/mail_osc/xnx3) ，以及 Jsoup 。
  
## 软件下载
支持Windows、Mac、Linux等系统。下载后解压，即可一键运行！<br/>
[下载网址 www.templatespider.zvo.cn](http://www.templatespider.zvo.cn/#rjxz)
<br/>

## 项目分支简介
#### 扒网站工具
![image](http://cdn.weiunity.com/site/341/news/b8adf77e43dd44d086325b53b4c3a79a.jpg)
看着网上哪个网站好，可用此软件将其下载，变为标准的 html 模版，自动分好图片、css、js、以及html页面。可以供网市场云建站、帝国CMS、织梦CMS等各大建站系统使用. [查看更多说明及步骤示例](http://www.wang.market/2712.html)

<br/>

#### 模版计算工具
![image](http://cdn.weiunity.com/site/341/news/edfa75bcf74044439cc14e1033b9b158.jpg)
将使用扒网站工具扒下来的html模版，或者网上自行下载的html模版、又或者你自己定制做的模版，导入进去。软件会自动帮您计算合成 网市场云建站系统 中可一键导入使用的模版（当然，您还是要微调的）. [查看更多说明及步骤示例](http://www.wang.market/4234.html)



## 扒网站工具使用步骤
1. 将要扒取的网站页面的网址粘贴进去
2. 点击左下方“开始抓取”按钮
3. 等待抓取完毕，自动打开下载好的文件夹


## 扒网站工具使用示例
例如，我要拔取 qiye1.wscso.com 这个网站中的首页、关于我们、新闻列表三个页面，拔取下来做成模板，扒网站工具中可以这么设置URL：

````
http://qiye1.wscso.com
http://qiye1.wscso.com/gongsijieshao.html
http://qiye1.wscso.com/xinwenzixun.html
````

设置如下图所示
![image](http://cdn.weiunity.com/site/254/news/20180126/1516934727962011819.png)

设置好后，点击左下角的“开始提取”按钮，便开始了自动扒这几个设置好的页面。

扒完后，会自动打开下载好的文件夹。例如，上面拔取的结果：

![image](http://cdn.weiunity.com/site/254/news/20180126/1516935019354059686.png)

可以看到，扒取下来的网页算是很规范的模板页。可以直接打开模板页进行浏览查看。


## 二次开发
1. 下载Ecipse。不知道如何下载，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600899&doc_id=1101390
2. eclipse中导入本git项目。 不会导入，可参考 https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3600882&doc_id=1101390
3. 运行。直接在 ````com.xnx3.spider.entry.java上右键-运行，即可运行。

## 配套-网市场云建站CMS建站系统-已贡献入华为云开发团队
[https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms)  
华为云OBS与网市场云建站CMS内容管理系统的深度整合，提供一个无技术门槛、可快速搭建100%自由度的网站。可做博客、文档、企业官网、电子简历…… 结合云计算的海量、安全、高可靠、低成本等优势，使各企业都能享受华为云所带来的技术变革。[优先建议采用此方式]

## 优秀开源项目及社区推荐
[https://github.com/featbit/featbit](https://github.com/featbit/featbit) 100% 开源的 Feature Management 平台。将软件交付风险在面向最终用户前降至最低，随便瞎写也不会有bug，加速软件创新的历史进度。FeatBit 赋能全团队交付、管理软件的超能力。

## 关于及交流
作者：管雷鸣  
微信：xnx3com
使用有问题可加作者微信并备注：扒网站工具进群， 我看到后便通过同时将你拉入微信交流群  


## 使用许可
本软件及源码仅用于技术学习、研究使用，禁止用于非法用途！


## 开源项目

致力于开源基础化信息建设，如有需要，可直接拿去使用。这里列出了我部分开源项目：

| 项目| star数量 | 简介 |  
| --- | --- | --- |
|[wangmarket CMS](https://gitee.com/mail_osc/wangmarket) | ![](https://gitee.com/mail_osc/wangmarket/badge/star.svg?theme=white) | [私有部署自己的SAAS建站系统](https://gitee.com/mail_osc/wangmarket)  |
|[obs-datax-plugins](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) | ![](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins/badge/star.svg?theme=white) | [Datax 的 华为云OBS 插件](https://gitee.com/HuaweiCloudDeveloper/obs-datax-plugins) |
| [templatespider](https://gitee.com/mail_osc/templatespider) | ![](https://gitee.com/mail_osc/templatespider/badge/star.svg?theme=white) | [扒网站工具，所见网站皆可为我所用](https://gitee.com/mail_osc/templatespider) |
|[FileUpload](https://gitee.com/mail_osc/FileUpload)| ![](https://gitee.com/mail_osc/FileUpload/badge/star.svg?theme=white ) | [文件上传，各种存储任意切换](https://gitee.com/mail_osc/FileUpload) |
| [cms client](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms) | ![](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms/badge/star.svg?theme=white) | [云服务深度结合无服务器建站](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-obs-website-wangmarket-cms)  |
| [kefu.js](https://gitee.com/mail_osc/kefu.js) | ![](https://gitee.com/mail_osc/kefu.js/badge/star.svg?theme=white ) | https://gitee.com/mail_osc/kefu.js | [在线聊天的前端框架](https://gitee.com/mail_osc/kefu.js)  | 
| [msg.js](https://gitee.com/mail_osc) | ![](https://gitee.com/mail_osc/msg/badge/star.svg?theme=white ) | [轻量级js消息提醒组件](https://gitee.com/mail_osc)  | 
| [translate.js](https://gitee.com/mail_osc/translate) | ![](https://gitee.com/mail_osc/translate/badge/star.svg?theme=white )  | [三行js实现 html 全自动翻译](https://gitee.com/mail_osc/translate)  | 
| [WriteCode](https://gitee.com/mail_osc/writecode) | ![](https://gitee.com/mail_osc/writecode/badge/star.svg?theme=white ) | [代码生成器，自动写代码](https://gitee.com/mail_osc/writecode)  | 
| [log](https://gitee.com/mail_osc/log) | ![](https://gitee.com/mail_osc/log/badge/star.svg?theme=white ) | [Java日志存储及读取](https://gitee.com/mail_osc/log) | 
| [layui translate](https://gitee.com/mail_osc/translate_layui) |  ![](https://gitee.com/mail_osc/translate_layui/badge/star.svg?theme=white ) | [Layui的国际化支持组件](https://gitee.com/mail_osc/translate_layui) |
| [http.java](https://gitee.com/mail_osc/http.java) |  ![](https://gitee.com/mail_osc/http.java/badge/star.svg?theme=white ) | [Java8轻量级http请求类](https://gitee.com/mail_osc/http.java) |
| [xnx3](https://gitee.com/mail_osc/xnx3) |  ![](https://gitee.com/mail_osc/xnx3/badge/star.svg?theme=white ) | [Java版按键精灵，游戏辅助开发](https://gitee.com/mail_osc/xnx3) |  
| [websocket.js](https://gitee.com/mail_osc/websocket.js)  | ![](https://gitee.com/mail_osc/websocket.js/badge/star.svg?theme=white ) | [js的WebSocket框架封装](https://gitee.com/mail_osc/websocket.js) |
| [email.java](https://gitee.com/mail_osc/email.java) | ![](https://gitee.com/mail_osc/email.java/badge/star.svg?theme=white ) | [邮件发送](https://gitee.com/mail_osc/email.java) | 
| [notification.js](https://gitee.com/mail_osc/notification.js) | ![](https://gitee.com/mail_osc/notification.js/badge/star.svg?theme=white ) | [浏览器通知提醒工具类](https://gitee.com/mail_osc/notification.js) | 
| [pinyin.js](https://gitee.com/mail_osc/pinyin.js) | ![](https://gitee.com/mail_osc/pinyin.js/badge/star.svg?theme=white ) | [JS中文转拼音工具类](https://gitee.com/mail_osc/pinyin.js) |
| [xnx3_weixin](https://gitee.com/mail_osc/xnx3_weixin) | ![](https://gitee.com/mail_osc/xnx3_weixin/badge/star.svg?theme=white ) | [Java 微信常用工具类](https://gitee.com/mail_osc/xnx3_weixin) |
| [xunxian](https://gitee.com/mail_osc/xunxian) | ![](https://gitee.com/mail_osc/xunxian/badge/star.svg?theme=white ) | [QQ寻仙的游戏辅助软件](https://gitee.com/mail_osc/xunxian) | 
| [wangmarket_shop](https://gitee.com/leimingyun/wangmarket_shop) | ![](https://gitee.com/leimingyun/wangmarket_shop/badge/star.svg?theme=white ) | [私有化部署自己的 SAAS 商城](https://gitee.com/leimingyun/wangmarket_shop) |
| [wm](https://gitee.com/leimingyun/wm) | ![](https://gitee.com/leimingyun/wm/badge/star.svg?theme=white ) | [Java开发框架及规章约束](https://gitee.com/leimingyun/wm) |
| [yunkefu](https://gitee.com/leimingyun/yunkefu) | ![](https://gitee.com/leimingyun/yunkefu/badge/star.svg?theme=white ) | [私有化部署自己的SAAS客服系统](https://gitee.com/leimingyun/yunkefu) |
| [javadoc](https://gitee.com/leimingyun/javadoc) | ![](https://gitee.com/leimingyun/javadoc/badge/star.svg?theme=white) | [根据标准的 JavaDoc 生成接口文档 ](https://gitee.com/leimingyun/javadoc) |
| [elasticsearch util](https://gitee.com/leimingyun/elasticsearch) | ![](https://gitee.com/leimingyun/elasticsearch/badge/star.svg?theme=white ) | [用sql方式使用Elasticsearch](https://gitee.com/leimingyun/elasticsearch) |
| [AutoPublish](https://gitee.com/leimingyun/sftp-ssh-autopublish) | ![](https://gitee.com/leimingyun/sftp-ssh-autopublish/badge/star.svg?theme=white ) | [Java应用全自动部署及更新](https://gitee.com/leimingyun/sftp-ssh-autopublish) |
| [aichat](https://gitee.com/leimingyun/aichat) | ![](https://gitee.com/leimingyun/aichat/badge/star.svg?theme=white ) | [智能聊天机器人](https://gitee.com/leimingyun/aichat) | 
| [yunbackups](https://gitee.com/leimingyun/yunbackups) | ![](https://gitee.com/leimingyun/yunbackups/badge/star.svg?theme=white ) | [自动备份文件到云存储及FTP等](https://gitee.com/leimingyun/yunbackups) |
| [chatbot](https://gitee.com/leimingyun/chatbot) | ![](https://gitee.com/leimingyun/chatbot/badge/star.svg?theme=white) | [智能客服机器人](https://gitee.com/leimingyun/chatbot) |
| [java print](https://gitee.com/leimingyun/printJframe) | ![](https://gitee.com/leimingyun/printJframe/badge/star.svg?theme=white ) | [Java打印及预览的工具类](https://gitee.com/leimingyun/printJframe) |
…………